@extends('layouts.blank')

@section('styles')
    <link rel="stylesheet" type="text/css" href="css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/creative.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
@endsection

@section('content')

<body id="page-top">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container clearfix">
            <div class="navbar-header">
                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                
                
                <a class="navbar-brand" href="{{ url('http://www.ies-sabadell.cat') }}">
                    <img style="height: 25px" src="{{asset('images/logo3.png')}}" alt="IES Sabadell">
                </a>
                <a class="navbar-brand" href="{{ url('/') }}">
                    Enquest.app
                </a>
               
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav navbar">
                   
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                     @if (Auth::user())
                        <li><a href="{{ url('/home') }}">Area Professors</a></li>
                    @endif
                    @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Entrar</a></li>
                    <li><a href="{{ url('/register') }}">Registrar-se</a></li>
                    @else
                    @if (Auth::user()->isAdmin() || Auth::user()->isEditor())
                    <li><a href="{{ url('/admin') }}">Administrar</a></li>
                    @endif
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/edituser') }}"><i class="fa fa-btn fa-edit"></i>Editar</a></li>
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Sortir</a></li>
                        </ul>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    <header>
        <div class="header-content">
            <div class="header-content-inner">
                <a href="{{ url('http://www.ies-sabadell.cat') }}">
                    <img style="width: 50%" src="{{asset('images/logo1.png')}}" alt="IES Sabadell">
                </a>
                <h1>Enquest.app</h1>
                <h2>Plataforma d'enquestes a l'alumnat</h2>
                <hr>
                <!-- Form user -->
                <form action="accesssurvey" method="post" role="form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <div class="form-group">
                            <label class="control-label" for="key">
                                <input type="text" class="form-control" id="key" name="key" placeholder="clau accés">
                            </label>
                        </div>
                        @if ($errors->any())
                        <div class="alert alert-danger fade-in">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Error!</strong> La clau introduïda no és vàlida.
                        </div>
                        @endif
                        @if (session('success'))
                            <div class="alert alert-success fade-in">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ session('success') }}
                            </div>
                        @endif
                        <p>Introduiu la clau d'accés que us a arribat al vostre correu per a participar en la enquesta.</p>
                    </div>
                    <button type="submit" class="btn btn-primary btn-xl page-scroll">Continuar</button>
                </form>
            </div>
        </div>
    </header>
</body>
@endsection
