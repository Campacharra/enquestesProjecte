<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<div class="panel-title pull-left">Assignatures</div>
		<div class="pull-right">
			<button class="btn btn-primary pull-right" data-toggle="modal" data-target="#addconcept-modal"><i class="fa fa-plus"></i> Assignatura</button>
		</div>
	            <!--modal-->
        <div id="addconcept-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Nova assignatura</h4>
                    </div>
                    <!-- add survey form --> 
                    <form action="createconcept" method="post">
                        <div class="modal-body">           
                            <div class="form-group">
                                <label for="name">Assignatura:</label>
                                <input type="text" class="form-control" name="name">
                            </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary btn-edit-user">Guardar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tancar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Nom</th>
						<th><span class="pull-right">Accions</span></th>
					</tr>
				</thead>
				<tbody>
					@foreach ($concepts as $concept)
						<tr>
							<td>{{ $concept->name }}</td>
							<td>
                                <div class="pull-right">
								    <button class="btn btn-sm btn-warning" data-toggle="modal" data-target="#editconcept-modal-{{ $concept->id }}"><i class="fa fa-edit"></i> editar</button>
								    <a class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteconcept-modal-{{ $concept->id }}"><i class="fa fa-trash-o"></i> eliminar</a>
                                </div>
							</td>
						</tr>
						<div id="deleteconcept-modal-{{ $concept->id }}" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">{{ $concept->name }}</h4>
                                    </div>
                                    <div class="modal-body">
                                        @if ( $concept->isUsed())
                                            <div class="alert alert-danger">
                                            <h3>ATENCIÓ:</h3>
                                            <p><strong>Aquesta assignatura te enquestes publicades, si elimines aquesta assignatura, s'esborraran totes les enquestes publicades amb els seus possibles resultats.</strong>
                                            </p>
                                            </div>
                                        @endif
                                        <p>Estàs segur/a de voler eliminar aquesta assignatura?</p>
                                       
                                    </div>
                                    <div class="modal-footer">
                                        <a href="deleteconcept/{{ $concept->id }}" class="btn btn-danger">Eliminar</a>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel·lar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Modal editar assignatures-->
                        <div id="editconcept-modal-{{$concept->id}}" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Editar {{$concept->name}}</h4>
                                    </div>
                                    <!-- add survey form --> 
                                    <form action="editconcept/{{$concept->id}}" method="post">
                                        <div class="modal-body">           
                                            <div class="form-group">
                                                <label for="name">Assignatura:</label>
                                                <input type="text" class="form-control" name="name" value="{{$concept->name}}">
                                            </div>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary btn-edit-concept">Editar</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tancar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>             
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="panel-footer"><span class="label label-primary">Total: {{ count($concepts) }}</span></div>
</div>