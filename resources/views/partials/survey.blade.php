<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<div class="panel-title pull-left">Enquestes</div>
		<div class="pull-right">
            <button class='btn btn-primary pull-right @if (Auth::user()->edit == false) disabled @endif' data-toggle="modal" data-target="#createsurvey-modal"><i class="fa fa-plus"></i> Enquesta</button>
		</div>
        <!--modal-->
            <div id="createsurvey-modal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Nova enquesta</h4>
                        </div>
                        <!-- add survey form --> 
                        <form action="createnew" method="post">
                            <div class="modal-body">           
                                <div class="form-group">
                                    <label for="name">Nom:</label>
                                    <input type="text" class="form-control" name="name" required>
                                </div>
                                <div class="form-group">
                                    <label for="type">Estudis:</label>
                                    <select name="survey-type" id="sel1" class="form-control">
                                    @foreach($categories as $category )
                                       <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary btn-edit-user">Guardar</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tancar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Enquesta</th>
						<th>Estudis</th>
						<th>Publicable</th>
						<th>Vegades publicada</th>
						<th><span class="pull-right">Accions</span></th>
					</tr>
				</thead>
				<tbody>
					@foreach ($surveys as $survey)
					<tr id="survey{{ $survey->id }}" data-id="{{ $survey->id }}">
						<td>{{ $survey->name }}</td>
						<td>{{ $survey->category->name }}</td>
						<td>
						@if(!$survey->outdated)
							<button type="button" class="btn btn-primary btn-sm btn-active-survey" value="{{ $survey->active}}">
								@if($survey->active)
									Si
								@else
									No
								@endif
							</button>
						@else
							<button type="button" class="btn btn-primary btn-sm btn-danger disabled">Obsoleta</button>
						@endif
							
						</td>
						<td>{{$survey->publishedSurveys->count()}}</td>
                        <td>
                        	<div class="pull-right">
                        		@if(!$survey->outdated)	
	                        		@if($survey->publishedSurveys->count() > 0)
	                        		<span>
										<a href="editsurvey/{{ $survey->id }}" class="btn btn-sm btn-primary "><i class="fa fa-eye"></i>Veure</a>
									</span>
									@endif
	                        	
	                        		@if($survey->publishedSurveys->count() == 0)
	                        		<span>
										<a href="editsurvey/{{ $survey->id }}" class="btn btn-sm btn-warning @if (Auth::user()->edit == false) disabled @endif"><i class="fa fa-edit"></i> Editar</a>
									<span>
									@else
									</span>
										<button data-toggle="modal" class="btn btn-sm btn-warning @if (Auth::user()->edit == false) disabled @endif" data-target="#updatesurvey-modal-{{$survey->id}}"><i class="fa fa-refresh"></i> Actualitzar</a></button>
									</span>
									@endif
								@endif
								<button class="btn btn-sm btn-danger @if (Auth::user()->edit == false) disabled @endif" data-toggle="modal" data-target="#deletesurvey-modal-{{ $survey->id }}"><i class="fa fa-trash-o"></i> eliminar</button>
							</div>
						</td>
					</tr>

					 <!--modal actualitza-->
		            <div id="updatesurvey-modal-{{$survey->id}}" class="modal fade" role="dialog">
		                <div class="modal-dialog">
		                    <!-- Modal content-->
		                    <div class="modal-content">
		                        <div class="modal-header">
		                            <button type="button" class="close" data-dismiss="modal">&times;</button>
		                            <h4 class="modal-title">Actualitzar enquesta</h4>
		                        </div>
		                        <!-- add survey form --> 
		                        <form action="{{url('/updatesurvey/'.$survey->id)}}" method="post">
		                            <div class="modal-body">  
		                            	<div class="alert alert-warning">
		                            		<h2>Atenció:</h2>
  											<p>Al actualitzar aquesta enquesta, s'en generarà una de nova a partir del contingut d'aquesta. Aquesta es mantindrà emmagatzemada per a consultar els seus resultats, però no es podrà tornar a publicar.</p>
										</div>
         
		                                <div class="form-group">
		                                    <label for="name">Nom:</label>
		                                    <input type="text" class="form-control" name="name" value="{{$survey->name}}" required>
		                                </div>
		                                <div class="form-group">
		                                    <label for="type">Estudis:</label>
		                                    <select name="survey-type" class="form-control">
		                                    @foreach($categories as $category )
		                                       <option value="{{$category->id}}" @if ($category->id === $survey->category_id) selected> @endif{{$category->name}}</option>
		                                    @endforeach
		                                    </select>
		                                </div>
		                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
		                            </div>
		                            <div class="modal-footer">
		                                <button type="submit" class="btn btn-primary btn-edit-user"><i class="fa fa-refresh"></i>Actualitzar</button>
		                                <button type="button" class="btn btn-default" data-dismiss="modal">Tancar</button>
		                            </div>
		                        </form>
		                    </div>
		                </div>
		            </div>
					<!-- Modal delete -->
					<div id="deletesurvey-modal-{{ $survey->id }}" class="modal fade" role="dialog">
						<div class="modal-dialog">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">{{ $survey->name }}</h4>
								</div>
								<div class="modal-body">
                                @if ($survey->publishedSurveys->count() > 0)
									<div class="alert alert-danger">
		                            	<h2>Atenció:</h2>
  											<p>Aquesta Enquesta ja ha estat publicada, si s'esborra es perdràn els possibles resultats.</p>
										</div>
                                @endif
                                    <p>Estàs segur de voler eliminar aquesta enquesta?</p>
                                   
								</div>
								<div class="modal-footer">
                                    <a href="deletesurvey/{{ $survey->id }}" class="btn btn-danger">Eliminar</a>
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancel·lar</button>
								</div>
							</div>
						</div>
					</div>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="panel-footer"><span class="label label-primary">Total: {{ count($surveys) }}</span></div>
</div>
