<div class="panel panel-default">
	<div class="panel-heading clearfix">
        <div class="panel-title pull-left">Clau d'accés per a nous usuaris</div>
    </div>
    <div class="panel-body">
        <!-- add survey form --> 
        <form action="updatekey" method="post">   

            <input type="hidden" name="_token" value="{{ csrf_token() }}">  

            <div class="form-group{{ $errors->has('privateKey') ? ' has-error' : '' }}">

                <div class="input-group">

                    <input type="text" class="form-control" name="privateKey" value="{{ $privateKey->first()->value }}">

                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
                    </span>

                </div>

                 @if ($errors->has('privateKey'))
                    <span class="help-block">
                        <strong>{{ $errors->first('privateKey') }}</strong>
                    </span>
                @endif
            </div>            
        </form>
        @if (session('editkeysuccess'))
            <div class="alert alert-success fade-in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ session('editkeysuccess') }}
            </div>
        @endif
    </div>	
</div>