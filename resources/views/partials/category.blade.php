<div class="panel panel-default">
	<div class="panel-heading clearfix">
        <div class="panel-title pull-left">Categories</div>
        <div class="pull-right">
            <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#addcategory-modal"><i class="fa fa-plus"></i> Categoria</button>
            <!--modal-->
            <div id="addcategory-modal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Nova categoria</h4>
                        </div>
                        <!-- add survey form --> 
                        <form action="createcategory" method="post">
                            <div class="modal-body">           
                                <div class="form-group">
                                    <label for="name">Categoria:</label>
                                    <input type="text" class="form-control" name="name">
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary btn-edit-user">Guardar</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tancar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Nom</th>
						<th><span class="pull-right">Accions</span></th>
					</tr>
				</thead>
				<tbody>
					@foreach ($categories as $category)
						<tr>
							<td>{{ $category->name }}</td>
							<td> 
                                <div class="pull-right">
                                    <button class="btn btn-sm btn-warning btn-edit-category" data-toggle="modal" data-target="#editcategory-modal-{{ $category->id }}"><i class="fa fa-edit"></i> editar</button>
								    <a class="btn btn-sm btn-danger btn-delete-category" data-toggle="modal" data-target="#deletecategory-modal-{{ $category->id }}"><i class="fa fa-trash-o"></i> eliminar</a>
                                </div>
							</td>
						</tr>
                        <!--Modal esborrar categoria-->
                        <div id="deletecategory-modal-{{ $category->id }}" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">{{ $category->name }}</h4>
                                    </div>
                                    <div class="modal-body">
                                        @if ( $category->isUsed())
                                            <div class="alert alert-danger">
                                            <h3>ATENCIÓ:</h3>
                                            <p><strong>Aquesta categoria te enquestes associades, si elimines aquesta categoria, s'esborraran totes les enquestes associades amb els seus possibles resultats.</strong>
                                            </p>
                                            </div>
                                        @endif
                                        <p>Estàs segur/a de voler eliminar aquesta categoria?</p>
                                       
                                    </div>
                                    <div class="modal-footer">
                                        <a href="deletecategory/{{ $category->id }}" class="btn btn-danger">Eliminar</a>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel·lar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Modal editar categoria-->
                        <div id="editcategory-modal-{{$category->id}}" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Editar {{$category->name}}</h4>
                                    </div>
                                    <!-- add survey form --> 
                                    <form action="editcategory/{{$category->id}}" method="post">
                                        <div class="modal-body">           
                                            <div class="form-group">
                                                <label for="name">Categoria:</label>
                                                <input type="text" class="form-control" name="name" value="{{$category->name}}">
                                            </div>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary btn-edit-user">Editar</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tancar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>             
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="panel-footer"><span class="label label-primary">Total: {{ count($categories) }}</span></div>
</div>