<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<div class="panel-title pull-left">Usuaris</div>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Nom</th>
						<th>Email</th>
						<th>Publicar</th>
						<th>Editar</th>
						<th>Supervisar</th>
						<th>Administrar</th>
						<th><span class="pull-right">Accions</span></th>
					</tr>
				</thead>
				<tbody>
					@foreach ($users as $user)
					<tr id="user{{ $user->id }}" data-id="{{ $user->id }}">
						<td>{{ $user->name }}</td>
						<td>{{ $user->email }}</td>
						<td>
							@if ($user->publish)
								<i class="fa fa-check-square-o fa-lg"></i>
							@else
								<i class="fa fa-square-o fa-lg"></i>
							@endif
						</td>
						<td>
							@if ($user->edit)
								<i class="fa fa-check-square-o fa-lg"></i>
							@else
								<i class="fa fa-square-o fa-lg"></i>
							@endif
						</td>
						<td>
							@if ($user->super)
								<i class="fa fa-check-square-o fa-lg"></i>
							@else
								<i class="fa fa-square-o fa-lg"></i>
							@endif
						</td>
						<td>
							@if ($user->admin)
								<i class="fa fa-check-square-o fa-lg"></i>
							@else
								<i class="fa fa-square-o fa-lg"></i>
							@endif
						</td>
						<td>
							<div class="pull-right">
								<a class="btn btn-sm btn-warning @if ($user->admin) disabled @endif" data-toggle="modal" data-target="#user-form-{{ $user->id }}"><i class="fa fa-edit"></i> editar</a>
								<a class="btn btn-sm btn-danger btn-delete-user @if ($user->admin) disabled @endif"><i class="fa fa-trash-o"></i> eliminar</a>
							</div>
						</td>
					</tr>
					<!-- Modal -->
					<div id="user-form-{{ $user->id }}" class="modal fade" role="dialog">
						<div class="modal-dialog">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Editar usuari</h4>
								</div>
								<!-- formulario para editar usuario -->
								<form action="user/update/{{ $user->id }}" method="post" role="form">
									<div class="modal-body">	
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<div class="form-group">
											<label for="name">Nom:</label>
											<input type="text" class="form-control" name="name" value="{{ $user->name }}" disabled>
										</div>
										<div class="form-group">
											<label for="email">Email address:</label>
											<input type="email" class="form-control " name="email" value="{{ $user->email }}" disabled>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" name="publish" value="true" @if ($user->publish) {{ "checked" }} @endif > Publicar</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" name="edit" value="true" @if ($user->edit) {{ "checked" }} @endif > Editar</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" name="super" value="true" @if ($user->super) {{ "checked" }} @endif > Supervisar</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" name="admin" value="true" @if ($user->admin) {{ "checked" }} @endif > Administrar</label>
										</div>
									</div>
									<div class="modal-footer">
										<button type="submit" class="btn btn-primary">Guardar</button>
										<button type="button" class="btn btn-default" data-dismiss="modal">
										Tancar</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="panel-footer"><span class="label label-primary">Total: {{ count($users) }}</span></div>
</div>