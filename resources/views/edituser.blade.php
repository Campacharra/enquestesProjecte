@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Editar Usuari</div>
                <div class="panel-body">
                    
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/editusername') }}">
                                {!! csrf_field() !!}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">Nom:</label>

                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="name" value="{{ Auth::user()->name }}">

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-btn fa-save"></i>Editar nom
                                        </button>
                                    </div>
                                </div>
                            </form>
                            @if (session('editnamesuccess'))
                                <div class="alert alert-success fade-in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ session('editnamesuccess') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/edituseremail') }}">
                                {!! csrf_field() !!}
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">Adreça d'E-mail</label>

                                    <div class="col-md-6">
                                        <input type="email" class="form-control" name="email" value="{{ Auth::user()->email}}">

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-btn fa-save"></i>Editar email
                                        </button>
                                    </div>
                                </div>
                            </form>
                            @if (session('editemailsuccess'))
                                <div class="alert alert-success fade-in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ session('editemailsuccess') }}
                                </div>
                            @endif
                        </div>
                    </div>
                        
                        
                     <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/edituserpassword') }}">
                            {!! csrf_field() !!}
                                {{--
                                <div class="form-group{{ $errors->has('badpwd') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">contrasenya</label>

                                    <div class="col-md-6">
                                        <input type="password" class="form-control" name="old-password" required>
                                        @if ($errors->has('badpwd'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('badpwd') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                --}}

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">Nova contrasenya</label>

                                    <div class="col-md-6">
                                        <input type="password" class="form-control" name="password">

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">Confirma  la nova contrasenya</label>

                                    <div class="col-md-6">
                                        <input type="password" class="form-control" name="password_confirmation">

                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-btn fa-save"></i>Editar contrasenya
                                        </button>
                                    </div>
                                </div>
                            </form>
                            @if (session('editpwdsuccess'))
                                <div class="alert alert-success fade-in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ session('editpwdsuccess') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
	<script type="text/javascript" src="{{ asset('js/functions.js') }}"></script>
@endsection
