@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <div class="col-md-12">
                    <p class="row">
                        <div class=" h2 text-center">{{ $survey->name }}</div>
                    </p>
                    <p class="row">
                        <div class="col-md-4 h4 text-center"><strong>Publicable:</strong> 
                            @if ($survey->active)
                            Si
                            @else
                            No
                            @endif
                        </div>
                        <div class="col-md-4 h4 text-center"><strong>Data de creació:</strong>      {{$survey->created_at->formatLocalized('%A %d %B %Y')}}
                        </div>
                        <div class="col-md-4 h4 text-center"><strong>última modificació:</strong>      {{$survey->updated_at->formatLocalized('%A %d %B %Y')}}
                        </div>
                    </p>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">

                    @foreach ($survey->questions as $question)
                        <div class="panel panel-default">
                            <div class="panel-body clearfix">
                                <div class="form-group">
                                    <h4><strong>{{ $question->question_text }}</strong></h4>
                                    <div>
                                        @if ($question->question_type === 'text')
                                        <textarea class="form-control" name="{{ $question->id }}" rows="4" disabled></textarea> 
                                        @endif

                                        @if ($question->question_type === 'single')
                                        <p>Tria una opció:</p>
                                            @foreach ($question->options as $option)
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="{{ $question->id }}" value="{{ $option->id }}" disabled> {{ $option->option_text }}
                                                </label>
                                            </div>
                                            @endforeach
                                        @endif

                                        @if ($question->question_type === 'multiple')
                                        <p>Tria les opcions que creguis:</p>
                                            @foreach ($question->options as $option)
                                            <p class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="{{ $question->id }}[]" value="{{ $option->id }}" disabled> {{ $option->option_text }}
                                                </label>
                                            </p>
                                            @endforeach
                                        @endif

                                        @if ($question->question_type === "single_other")
                                        <p>Tria una opció:</p>
                                            @foreach ($question->options as $option)
                                                @if(!$option->other)
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="{{ $question->id }}" value="{{ $option->id }}" disabled> {{ $option->option_text }}
                                                    </label>
                                                </div>
                                                @endif
                                            @endforeach

                                            @foreach ($question->options as $option)
                                                @if ($option->other)
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="{{ $question->id }}" value="{{ $option->id }}" class="other-radio" disabled> {{ $option->option_text }}
                                                        <input id="other-{{$option->id}}" class="form-control" type="text" name="other" disabled>
                                                    </label>
                                                </div>
                                                @endif
                                            @endforeach
                                        @endif
                                        
                                        @if ($question->question_type === 'numeric')
                                        <p class="text-center">
                                            <span style="margin-right: 20px"><strong>{{$question->options[0]->numeric_min_text}}</strong></span>
                                            @foreach ($question->options as $option)
                                            <label class="radio-inline">
                                                <input type="radio" name="{{ $question->id }}" value="{{ $option->id }}" readonly> {{ $option->option_text}}
                                            </label>
                                            @endforeach
                                            <span style="margin-left: 20px"><strong>{{$question->options[0]->numeric_max_text}}</strong></span>
                                        </p>
                                        @endif

                                    </div>  
                                </div>
                                @if(Auth::user()->edit && count($survey->publishedSurveys) == 0) 
                                    <div class="form-group pull-right">
                                        <button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#editquestion-modal-{{$question->id}}"><i class="fa fa-edit"></i> Editar</button>
                                        <a href="{{ url("/deletequestion/".$question->id) }}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> eliminar</a>
                                           
                                    </div>    
                                @endif
                            </div>
                        </div> 

                        <!--Modal editar pregunta -->

                        <div id="editquestion-modal-{{$question->id}}" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Editar pregunta</h4>
                                    </div>
                                    <form action={{ url("/editquestion/".$question->id) }} method="post">
                                        <div class="modal-body">
                                            
                                            <div class="form-group">
                                                <label for="question" class="control-label">Pregunta:</label>
                                                <input type="text" class="form-control" name="question" value="{{$question->question_text}}" required>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="answer-type" class="control-label">Tipus de resposta:</label>
                                                <select name="answer-type" id="answerTypeSelection-{{$question->id}}" class="form-control" onchange='showAnswerType(this, "{{$question->id}}")' required>
                                                    <option value="">Tria un tipus de resposta</option>
                                                    <option value="text" @if ($question->question_type == 'text') selected @endif> Resposta de text</option>
                                                    <option value="single" @if ($question->question_type == 'single') selected @endif>Opció de resposta única</option>
                                                    <option value="single_other" @if ($question->question_type == 'single_other') selected @endif>Opció de resposta única o text</option>
                                                    <option value="multiple" @if ($question->question_type == 'multiple') selected @endif>Opció de resposta múltiple</option>
                                                    <option value="numeric" @if ($question->question_type == 'numeric') selected @endif>Puntuació</option>
                                                </select>
                                            </div>
                                           
                                            <div id="options-{{$question->id}}" class="form-group @if ($question->question_type == 'numeric' || $question->question_type == 'text') hidden @endif">          
                                                <div class="panel panel-default">
                                                    <div class="panel-heading clearfix">
                                                        <h3 class="panel-title pull-left" >Opcions</h3>
                                                        <button type="button" class="btn btn-primary btn-sm pull-right" onclick="addOption(0, '', '{{$question->id}}')">afegir</button>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="answer-options-{{$question->id}}">
                                                        @if ($question->question_type === "single" || $question->question_type === "multiple" || $question->question_type === "single_other")
                                                            @foreach ($question->options as $option)
                                                                @if (!$option->other)
                                                                <div class="option form-group input-group">
                                                                    <input class="form-control" name="option_text_0[]" type="text" value="{{$option->option_text}}" required>
                                                                    <span class="input-group-btn">'
                                                                        <button type="button" class="btn btn-primary" onclick="removeOption(this)">-</button>'
                                                                    </span>
                                                                </div>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                        </div>
                                                        <div class="other-option-{{$question->id}}">
                                                            <div class="option form-group">
                                                            @if ($question->question_type === "single_other")
                                                                @foreach ($question->options as $option)
                                                                    @if ($option->other)
                                                                    <label>opció de resposta de text
                                                                    
                                                                        <input class="form-control" name="option_text_other" type="text" value="{{$option->option_text}}" required >
                                                                    
                                                                    </label>
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            <div id="numeric-{{$question->id}}" class="form-group @if ($question->question_type != 'numeric') hidden @endif">
                                                <label for="rating-type" class="control-label">Puntuació sobre:</label>
                                                <select name="rating-type" class="form-control">
                                                    @for ($i = 1; $i < 11; $i++)
                                                        <option value="{{$i}}" 
                                                        @if($question->question_type == 'numeric')
                                                            @if($question->options[0]->rating == $i) selected @endif
                                                        @endif
                                                        >{{$i}}
                                                        </option>
                                                    @endfor
                                                </select>
                                                <label for="min-text" class="control-label">Text per a puntuació mínima:</label>
                                                <input type="text" class="form-control" name="min-text" @if($question->question_type == 'numeric') value= "{{$question->options[0]->numeric_min_text}}"@endif>
                                                <label for="max-text" class="control-label">Text per a puntuació màxima:</label>
                                                <input type="text" class="form-control" name="max-text" @if($question->question_type == 'numeric') value= "{{$question->options[0]->numeric_max_text}}"@endif>
                                            </div>   
          
                                        </div>
                                        <div class="modal-footer clearfix">
                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> Editar</button>
                                                <button type="button" onclick="location.reload()" class="btn btn-default" data-dismiss="modal">Cancel·lar</button>
                                            </div>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        </div>
                                    </form>      
                                </div>
                            </div>
                        </div>

                        @endforeach
                    </div>
                </div>
            </div> 
            <div class="panel-footer">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                    @if(Auth::user()->edit && count($survey->publishedSurveys) == 0) 
                        <button type="button" class="btn btn-primary pull-center" data-toggle="modal" data-target="#newquestion-modal"><i class="fa fa-plus"></i> Pregunta</button>
                        
                    @endif
                    @if(count($survey->publishedSurveys) == 0)
                        <a href="{{url('/admin')}}" class="btn btn-default pull-right"><i class="fa fa-arrow-left"></i>Tornar</a>
                    @else
                        <a href="javascript:history.back()" class="btn btn-default pull-right"><i class="fa fa-arrow-left"></i>Tornar</a>
                    @endif
                    </div>
                </div>    
            
            </div>  

        </div>
    </div>
</div>


<div id="newquestion-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Nova pregunta</h4>
            </div>
            <form action={{ url("/savequestion/".$survey->id) }} method="post">
                <div class="modal-body">
                    
                    <div class="form-group">
                        <label for="question" class="control-label">Pregunta:</label>
                        <input type="text" class="form-control" name="question" required>
                    </div>
                    
                    <div class="form-group">
                        <label for="answer-type" class="control-label">Tipus de resposta:</label>
                        <select name="answer-type" id="answerTypeSelection" class="form-control" onchange='showAnswerType(this, "new")' required>
                            <option value="">Tria un tipus de resposta</option>
                            <option value="text">Resposta de text</option>
                            <option value="single">Opció de resposta única</option>
                            <option value="single_other">Opció de resposta única o text</option>
                            <option value="multiple">Opció de resposta múltiple</option>
                            <option value="numeric">Puntuació</option>
                        </select>
                    </div>
             
                    <div id="options-new" class="form-group hidden">          
                        <div class="panel panel-default">
                            <div class="panel-heading clearfix">
                                <h3 class="panel-title pull-left" >Opcions</h3>
                                <button type="button" class="btn btn-primary btn-sm pull-right" onclick='addOption(0, "", "new")'>afegir</button>
                            </div>
                            <div class="panel-body">
                                <div class="answer-options-new">
                                </div>
                                <div class="other-option-new">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
    

                    <div id="numeric-new" class="form-group hidden">
                        <label for="rating-type" class="control-label">Puntuació sobre:</label>
                        <select name="rating-type" id="ratingSelection" class="form-control">
                            @for ($i = 1; $i < 11; $i++)
                              <option value={{$i}}>{{$i}}</option>
                            @endfor
                        </select>
                        <label for="min-text" class="control-label">Text per a puntuació mínima:</label>
                        <input type="text" class="form-control" name="min-text">
                        <label for="max-text" class="control-label">Text per a puntuació màxima:</label>
                        <input type="text" class="form-control" name="max-text">
                    </div>             
                </div>
                <div class="modal-footer clearfix">
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Afegir</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel·lar</button>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </div>
            </form>      
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/functions.js') }}"></script>
@endsection
