@extends('layouts.app')

@section('content')
<div class="container">

	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			@if ($errors->has('msg') || $errors->has('unique')) 
				<div class="alert alert-danger">
					 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  					<strong>Error!</strong> {{ $errors->first() }}
				</div>
			@endif
		</div>
	</div>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">

        	@if(Auth::user()->admin)
				@include('partials.key')								
			@endif

			<ul class="nav nav-pills well">
				<li class="active"><a data-toggle="pill" href="#surveys">Enquestes</a></li>
				<li><a data-toggle="pill" href="#categories">Categories</a></li>
				<li><a data-toggle="pill" href="#concepts">Assignatures</a></li>
				<li><a data-toggle="pill" href="#levels">Nivells</a></li>
				<li><a data-toggle="pill" href="#groups">Grups</a></li>
				@if(Auth::user()->admin)
					<li><a data-toggle="pill" href="#users">Usuaris</a></li>		
				@endif
			</ul>

			<div class="tab-content" style="border: 0px;">
				<div id="surveys" class="tab-pane fade in active">
					@include('partials.survey')
				</div>
				<div id="categories" class="tab-pane fade">
					@include('partials.category')
				</div>
				<div id="concepts" class="tab-pane fade">
					@include('partials.concept')
				</div>
				<div id="levels" class="tab-pane fade">
					@include('partials.level')
				</div>
				<div id="groups" class="tab-pane fade">
					@include('partials.group')
				</div>
				@if(Auth::user()->admin)
					<div id="users" class="tab-pane fade">
						@include('partials.user')
					</div>				
				@endif
			</div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
	<script type="text/javascript" src="{{ asset('js/functions.js') }}"></script>
@endsection
