@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <div class="col-md-12">
                     <p class="row">
                        <img class="center-block" style="width: 30%" src="{{asset('images/logo1.png')}}" alt="IES Sabadell">
                    </p>
                    <p class="row">
                        <div class=" h2 text-center">{{ $pubSurvey->survey->name }}</div>
                    </p>
                    <p class="row">
                        <div class="h4 text-center"><strong>Categoria:</strong> {{ $pubSurvey->survey->category->name }} </div>
                        <div class=" h4 text-center"><strong>Assignatura:</strong> {{ $pubSurvey->concept->name }} </div>
                        <div class="2 h4 text-center"><strong>Curs:</strong> {{ $pubSurvey->level->name }} </div>
                        <div class=" h4 text-center"><strong>Grup:</strong> {{ $pubSurvey->group->name }} </div>
                        <div class=" h4 text-center"><strong>Professor:</strong> {{ $pubSurvey->user->name }} </div>
                    </p>
                </div>
            </div>
            <form role="form" action={{ url("/saveanswer/".$pubSurvey->id."/".$intent->id)}} method="post">
                <div class="panel-body">
                    
                            
                    @foreach ($pubSurvey->survey->questions as $question)
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h4><strong>{{ $question->question_text }}</strong></h4>
                            <div>
                                @if ($question->question_type === 'text')
                                    <textarea class="form-control" name="{{ $question->id }}" rows="4"></textarea> 
                                @endif

                                @if ($question->question_type === 'single')
                                    <p>Tria una opció:</p>
                                    @foreach ($question->options as $option)
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="{{ $question->id }}" value="{{ $option->id }}" required> {{ $option->option_text }}
                                        </label>
                                    </div>
                                    @endforeach
                                @endif

                                @if ($question->question_type === 'multiple')
                                    <p>Tria les opcions que creguis:</p>
                                    @foreach ($question->options as $option)
                                    <p class="checkbox">
                                        <label>
                                            <input type="checkbox" name="{{ $question->id }}[]" value="{{ $option->id }}"> {{ $option->option_text }}
                                        </label>
                                    </p>
                                    @endforeach
                                @endif

                                @if ($question->question_type === "single_other")
                                     <p>Tria una opció:</p>
                                    @foreach ($question->options as $option)
                                        @if(!$option->other)
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="{{ $question->id }}" value="{{ $option->id }}" required> {{ $option->option_text }}
                                                </label>
                                            </div>
                                        @endif
                                    @endforeach

                                    @foreach ($question->options as $option)
                                        @if ($option->other)
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="{{ $question->id }}" value="{{ $option->id }}" class="other-radio" required> {{ $option->option_text }}
                                            <input id="other-{{$option->id}}" class="form-control" type="text" name="other">
                                                </label>
                                            </div>
                                        @endif
                                    @endforeach
                                @endif

                                @if ($question->question_type === 'numeric')
                                    <p class="text-center">
                                        <span style="margin-right: 20px"><strong>{{$question->options[0]->numeric_min_text}}</strong></span>
                                        @foreach ($question->options as $option)
                                        <label class="radio-inline">
                                            <input type="radio" id="inlineRadio1" name="{{ $question->id }}" value="{{ $option->id }}" required> {{ $option->option_text}}
                                        </label>
                                        @endforeach
                                        <span style="margin-left: 20px"><strong>{{$question->options[0]->numeric_max_text}}</strong></span>
                                    </p>
                                @endif

                            </div>  
                        </div> 
                    </div>                         
                    @endforeach             
                </div> 
                <div class="panel-footer">
                    <button type="submit" class="btn btn-primary center-block">Enviar respostes</button>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </div>  
            </form>
        </div>
    </div>
</div>


@endsection