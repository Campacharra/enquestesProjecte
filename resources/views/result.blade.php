@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<link rel="stylesheet" href="{{ asset('css/style2.css') }}">
@endsection

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			@if ($errors->has('msg')) 
				<div class="alert alert-info">
					 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  					<strong>Info!</strong> {{ $errors->first('msg') }}
				</div>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					Enquestes
					<button class="btn btn-xs btn-primary pull-right" data-toggle="modal" data-target="#filter-form"><i class="fa fa-filter"></i> Filtrar</button>
					<!-- Modal -->
						<div id="filter-form" class="modal fade" role="dialog">
							<div class="modal-dialog">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">Seleccionar filtres</h4>
									</div>
									<!-- formulario para seleccionar los filtros para encuestas -->
									<form action="{{ url('filtersurvey') }}" method="post" role="form">
										<div class="modal-body">
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<div class="form-group">
												<label for="category">Categoria:</label>
												<select id="category" class="form-control" name="category">
													<option value="0">Tots</option>
													@foreach ( $categories as $category )
														<option value="{{ $category->id }}">{{ $category->name }}</option>
													@endforeach
												</select>
											</div>
											<div class="form-group">
												<label for="user">Professor:</label>
												<select id="user" class="form-control" name="user">
													<option value="0">Tots</option>
													@foreach ( $users as $user )
														<option value="{{ $user->id }}">{{ $user->name }}</option>
													@endforeach
												</select>
											</div>
											<div class="form-group">
												<label for="concept">Assignatura:</label>
												<select id="concept" class="form-control" name="concept">
													<option value="0">Tots</option>
													@foreach ( $concepts as $concept )
														<option value="{{ $concept->id }}">{{ $concept->name }}</option>
													@endforeach
												</select>
											</div>
											<div class="form-group">
												<label for="level">Nivell:</label>
												<select id="level" class="form-control" name="level">
													<option value="0">Tots</option>
													@foreach ( $levels as $level )
														<option value="{{ $level->id }}">{{ $level->name }}</option>
													@endforeach
												</select>
											</div>
											<div class="form-group">
												<label for="group">Grup:</label>
												<select id="group" class="form-control" name="group">
													<option value="0">Tots</option>
													@foreach ( $groups as $group )
														<option value="{{ $group->id }}">{{ $group->name }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="modal-footer">
											<button type="submit" class="btn btn-primary">Filtrar</button>
											<button type="button" class="btn btn-default" data-dismiss="modal">Cancel·lar</button>
										</div>
									</form>
								</div>
							</div>
						</div>
				</div>
				<ul class="list-group">
				@foreach ($pubSurveys as $pubSurvey)
					<li class="list-group-item"><span class="label label-warning">{{ $pubSurvey->created_at->formatLocalized('%d %B %Y') }}</span> <a href="{{ url('viewsurvey/' . $pubSurvey->id ) }}"> {{ $pubSurvey->survey->name }}</a></li>
				@endforeach
				</ul>	
			</div>
		</div>
		<div class="col-md-9">
			<div class="well">
				<p class="row">
                 	<div class=" h2 text-center">{{ $pubSurvey->survey->name }}</div>
                 </p>
                 <hr>
				<div class="row">
					<div class="col-md-4">
						<p><strong>Professor:</strong> {{ $selectedSurvey->user->name }}</p>
						<p><strong>Publicada:</strong> {{ $selectedSurvey->created_at->formatLocalized('%d %B %Y') }}</p>
					</div>
					<div class="col-md-4">
						<p><strong>Categoria:</strong> {{ $selectedSurvey->survey->category->name }}</p>
						<p><strong>Assignatura:</strong> {{ $selectedSurvey->concept->name }}</p>
					</div>
					<div class="col-md-4">
						<p><strong>Nivell:</strong> {{ $selectedSurvey->level->name }}</p>
						<p><strong>Grup:</strong> {{ $selectedSurvey->group->name }}</p>
					</div>
				</div>			
			</div>
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							@foreach ($selectedSurvey->survey->questions as $question)
							<div class="panel panel-default">
								<div class="panel-body">
									<h3>{{ $question->question_text }}</h2>
									<hr>
									<h3 class="text-primary">Respostes:</h3>
									@if ($question->question_type == "text")
										<ul class="list-group">				
										@foreach ($question->answers as $answer)
											<li class="list-group-item"> {{ $answer->answer }}</li>
										@endforeach
										</ul>
									@endif
									<?php $sum = 0; ?>
									@if ($question->question_type == "numeric")
										<?php 
											foreach ($question->answers as $answer){
												$op = $question->options->find($answer->answer);
												$sum += $op->option_text;
											}	
										?>
										<ul class="nav nav-tabs nav-justified">
									        <li class="active"><a data-toggle="tab" href="#menu-{{ $question->id }}-1"><i class="fa fa-bar-chart" aria-hidden="true"></i></a></li>
									        <li><a data-toggle="tab" href="#menu-{{ $question->id }}-2" class="tab-2"><i class="fa fa-pie-chart" aria-hidden="true"></i></a></li>
								         </ul>

					                    <div class="tab-content clearfix">
					                        <div id="menu-{{ $question->id }}-1" class="tab-pane fade in active"> 
					                            <div id="chart-{{ $question->id }}"></div>
					                        </div>
					                        <div id="menu-{{ $question->id }}-2" class="tab-pane fade in active">
					                            <div id="donut-{{ $question->id }}" class="donut"></div>
					                        </div>
					                    </div>
					                    @if(count($question->answers) > 0)
											<h3 class="text-@if($sum>=5)success  @endif">Mitjana: {{ ($sum) / count($question->answers) }}	</h3> 
										@endif	
					
									@endif
									@if ($question->question_type == "single")
										<ul class="nav nav-tabs nav-justified">
									        <li class="active"><a data-toggle="tab" href="#menu-{{ $question->id }}-1"><i class="fa fa-bar-chart" aria-hidden="true"></i></a></li>
									        <li><a data-toggle="tab" href="#menu-{{ $question->id }}-2" class="tab-2"><i class="fa fa-pie-chart" aria-hidden="true"></i></a></li>
								         </ul>

					                    <div class="tab-content clearfix">
					                        <div id="menu-{{ $question->id }}-1" class="tab-pane fade in active"> 
					                            <div id="chart-{{ $question->id }}"></div>
					                        </div>
					                        <div id="menu-{{ $question->id }}-2" class="tab-pane fade in active">
					                            <div id="donut-{{ $question->id }}" class="donut"></div>
					                        </div>
					                    </div>		
									@endif
									
					                @if ($question->question_type == "multiple")
										<ul class="nav nav-tabs nav-justified">
									        <li class="active"><a data-toggle="tab" href="#menu-{{ $question->id }}-1"><i class="fa fa-bar-chart" aria-hidden="true"></i></a></li>
									        <li><a data-toggle="tab" href="#menu-{{ $question->id }}-2" class="tab-2"><i class="fa fa-pie-chart" aria-hidden="true"></i></a></li>
								         </ul>

					                    <div class="tab-content clearfix">
					                        <div id="menu-{{ $question->id }}-1" class="tab-pane fade in active"> 
					                            <div id="chart-{{ $question->id }}"></div>
					                        </div>
					                        <div id="menu-{{ $question->id }}-2" class="tab-pane fade in active">
					                            <div id="donut-{{ $question->id }}" class="donut"></div>
					                        </div>
					                    </div>		
									@endif
					                
									@if ($question->question_type == "single_other")
										<ul class="nav nav-tabs nav-justified">
									        <li class="active"><a data-toggle="tab" href="#menu-{{ $question->id }}-1"><i class="fa fa-bar-chart" aria-hidden="true"></i></a></li>
									        <li><a data-toggle="tab" href="#menu-{{ $question->id }}-2" class="tab-2"><i class="fa fa-pie-chart" aria-hidden="true"></i></a></li>
								         </ul>

					                    <div class="tab-content clearfix">
					                        <div id="menu-{{ $question->id }}-1" class="tab-pane fade in active"> 
					                            <div id="chart-{{ $question->id }}"></div>
					                        </div>
					                        <div id="menu-{{ $question->id }}-2" class="tab-pane fade in active">
					                            <div id="donut-{{ $question->id }}" class="donut"></div>
					                        </div>
					                    </div>		
										<ul class="list-group">	Altres Respostes:			
										@foreach ($question->answers as $answer)
											@if($answer->answer_other)
												<li class="list-group-item"> {{ $answer->answer_other }}</li>
											@endif
										@endforeach
										</ul>					
									@endif
								</div>	
							</div>
							@endforeach	
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<a href="{{ url('/home') }}" class="btn btn-default">Tornar</a>
				</div>
			</div>
		</div>
	</div>	
</div>
@endsection

@section('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
<script src="{{asset('js/functions.js')}}"></script>
<script src="{{asset('js/encoder.js')}}"></script>
<script type="text/javascript" charset="UTF-8">
@foreach ($selectedSurvey->survey->questions as $question)
	if ($('#chart-{{ $question->id }}').length ) {
		Morris.Bar({
			element: 'chart-{{ $question->id }}',
			data: [
				@foreach ($question->options as $option)
					{ y : "{{ html_entity_decode($option->option_text) }}", respostes : {{ $option->countAnswers($selectedSurvey) }} },
				@endforeach
			],
			xkey: 'y',
			ykeys: ['respostes'],
			labels: ['respostes']
		});
	}
    
    if ($('#donut-{{ $question->id }}').length ) {
		Morris.Donut({
			element: 'donut-{{ $question->id }}',
			data: [
				@foreach ($question->options as $option)
					{ label : "{{ $option->option_text }}", value : "{{ $option->countAnswers($selectedSurvey)}}" },
				@endforeach
			]	
		});
	}
@endforeach
</script>
<script type="text/javascript" charset="UTF-8">
	function decodeHtml(html) {
	    var txt = document.createElement("textarea");
	    txt.innerHTML = html;
	    return txt.value;
	}
	$(document).ready(function(){
		Encoder.EncodeType = "entity";

		$("tspan").each(function(){
			if(Encoder.hasEncoded($(this).text())) {
	         	var str = Encoder.htmlDecode($(this).text());
	         	$(this).text(str);
         	}
    	});
    	$( "path" ).hover(function() {
    		$id = $(this).attr('href');
	    	$('tspan').each(function(){ 
	    		if(Encoder.hasEncoded($(this).text())) {
		         	var str = Encoder.htmlDecode($(this).text());
		         	$(this).text(str);
         		}
	    	});
    	});
	});


</script>
<script src="{{asset('js/functions.js')}}"></script>
@endsection