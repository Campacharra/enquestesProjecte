@extends('layouts.app')

@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
@endsection

@section('content')

<div class="container">

	<div class="row">
		<div class="col-md-12">
			@if ($errors->has('msg')) 
				<div class="alert alert-danger">
					 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  					<strong>Error!</strong> {{ $errors->first('msg') }}
				</div>
			@endif
		</div>
	</div>
	<div class="panel panel-default clearfix">
		<div class="panel-body">
			<div class="row">
				<ul class="nav nav-pills nav-justified">
					<li class="active"><a data-toggle="pill" href="#surveys">Enquestes</a></li>
					<li><a data-toggle="pill" href="#results">Resultats</a></li>
				</ul>
			</div>
				
			<div class="row">
				<div class="tab-content">
					<div id="surveys" class="tab-pane fade in active">
						<div class="well">
							<h3>Enquestes disponibles per publicar.</h3>
						</div>
						@include('home/survey')
					</div>
					<div id="results" class="tab-pane fade">
						<div class="well">
							<h3>Resultats de les enquestes.</h3>
						</div>
						@include('home/result')
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
