<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>			
				<th>Nom</th>
				<th>Categoria</th>
				<th>Data creació</th>
				<th>Data actualització</th>
				<th>Accions</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($surveys as $survey)
				<tr>
					<td>{{ $survey->name }}</td>
					<td>{{ $survey->category->name }}</td>
					<td>{{ $survey->created_at }}</td>
					<td>{{ $survey->updated_at }}</td>
					<td>
						@if(Auth::user()->publish)
							<a href="editsurvey/{{ $survey->id }}" class="btn btn-sm btn-primary btn-xs"><i class="fa fa-eye"></i>Veure</a>
						
							<a class="btn btn-sm btn-primary btn-xs" data-toggle="modal" data-target="#survey-form-{{ $survey->id }}"><i class="fa f-btn fa-rss"></i> Publicar</a>
						@endif
					</td>
				</tr>
				<!-- Modal -->
					<div id="survey-form-{{ $survey->id }}" class="modal fade" role="dialog">
						<div class="modal-dialog">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Publicar enquesta</h4>
								</div>
								<!-- formulario para editar usuario -->
								<form action="publishsurvey/{{ $survey->id }}" method="post" role="form">
									<div class="modal-body">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<div class="form-group">
											<label for="user">Professor:</label>
											<select id="user" class="form-control" name="user">
												@foreach ( $users as $user )
													<option value="{{ $user->id }}">{{ $user->name }}</option>
												@endforeach
											</select>
										</div>
										<div class="form-group">
											<label for="concept">Assignatura:</label>
											<select id="concept" class="form-control" name="concept">
												@foreach ( $concepts as $concept )
													<option value="{{ $concept->id }}">{{ $concept->name }}</option>
												@endforeach
											</select>
										</div>
										<div class="form-group">
											<label for="level">Curs:</label>
											<select id="level" class="form-control" name="level">
												@foreach ( $levels as $level )
													<option value="{{ $level->id }}">{{ $level->name }}</option>
												@endforeach
											</select>
										</div>
										<div class="form-group">
											<label for="group">Grup:</label>
											<select id="group" class="form-control" name="group">
												@foreach ( $groups as $group )
													<option value="{{ $group->id }}">{{ $group->name }}</option>
												@endforeach
											</select>
										</div>
										<div class="form-group">
											<label for="emails">Emails:</label>
											<textarea id="emails" class="form-control" rows="4" cols="50" name="emails"></textarea>
										</div>
										{{--
		                                <div class="form-group">
		                                    <div class="panel panel-default">
		                                        <div class="panel-heading clearfix">
		                                            <div class="panel-title pull-left" >Preguntes anexes </div>
		                                            <div class="pull-right">
		                                                <button id="add-annex-question-btn" type="button" onclick="addAnnexQuestion()" class="btn btn-sm btn-primary">afegir</button>
		                                            </div>
		                                        </div>
		                                        <div id="annex-questions" class="panel-body">
		                                            
		                                        </div>
		                                    </div>
		                                </div>
                                    --}}
									</div>
									<div class="modal-footer">
										<button type="submit" class="btn btn-primary">Publicar</button>
										<button type="button" class="btn btn-default" data-dismiss="modal">Cancel·lar</button>
									</div>
								</form>
							</div>
						</div>
					</div>
			@endforeach
		</tbody>
	</table>
</div>