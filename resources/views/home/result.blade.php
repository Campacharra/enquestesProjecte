<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Data de publicació</th>
				<th>Nom</th>
				<th>Professor</th>
				<th>Materia</th>
				<th>Grup</th>
				<th>Nivell</th>
				<th>Respostes</th>
				<th>Accions</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($pubSurveys as $pubSurvey)
				<tr>
					<td>{{ $pubSurvey->created_at->formatLocalized('%A %d %B %Y') }}</td>
					<td>{{ $pubSurvey->survey->name }}</td>
					<td>{{ $pubSurvey->user->name }}</td>
					<td>{{ $pubSurvey->concept->name }}</td>
					<td>{{ $pubSurvey->group->name }}</td>
					<td>{{ $pubSurvey->level->name }}</td>
					<td><span class="badge">{{ $pubSurvey->count }}</span></td>
					<td>
						@if($pubSurvey->survey->active == true && $pubSurvey->survey->outdated == false)
							<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#resend-form-{{ $pubSurvey->id }}"><i class="fa fa-btn fa-envelope"></i> Enviar</button>
						@endif
						<!-- Modal -->
						<div id="resend-form-{{ $pubSurvey->id }}" class="modal fade" role="dialog">
							<div class="modal-dialog">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">Enviar més emails</h4>
									</div>
									<!-- formulario para enviar emails -->
									<form action="resendsurvey/{{ $pubSurvey->id }}" method="post" role="form">
										<div class="modal-body">
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<div class="alert alert-info fade-in">
				                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				                                <strong>Info!</strong> Separa cada direcció mitjançant el símbol ";".
				                            </div>
											<div class="form-group">
												<label for="emails">Emails:</label>
												<textarea id="emails" class="form-control" rows="4" cols="50" name="emails"></textarea>
											</div>
										</div>
										<div class="modal-footer">
											<button type="submit" class="btn btn-primary">Enviar</button>
											<button type="button" class="btn btn-default" data-dismiss="modal">Cancel·lar</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						<a href="viewsurvey/{{ $pubSurvey->id }}" class="btn btn-primary btn-xs"><i class="fa fa-btn fa-bar-chart"></i> Veure</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>