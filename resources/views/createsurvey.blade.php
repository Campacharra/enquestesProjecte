@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Create new survey</div>
                	<form action="createnew" method="post">
                		<div id="form-group">
                			<label for="name" class="col-lg-2 control-label">Form name:</label>
                			<div class="col-lg-10">
                				<input type="text" class="form-control" name="name">
                			</div>
                		</div>
                		<div id="form-group">
                			<label for="type" class="col-lg-2 control-label">Studies:</label>
                			<div class="col-lg-2">
                				<select name="survey-type" id="sel1" class="form-control">
                        @foreach($categories as $category )
                					<option value="{{$category->id}}">{{$category->name}}</option>
                				@endforeach
                				</select>
                			</div>
                		</div>
                		<div class="form-actions">
                			<button type="submit" class="btn btn-primary">Create</button>
                		</div>
                		<input type="hidden" name="_token" value="{{ csrf_token() }}">

                	</form>



            </div>
        </div>
    </div>
</div>


@endsection
