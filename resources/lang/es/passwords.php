<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La contrasenya ha de tenir 6 caràcters com a mínim.',
    'reset' => 'La contrasenya ha estat restablida!',
    'sent' => "L'enllaç per restablir la contrasenya ha estat enviat!",
    'token' => 'El token per restablir la contrasenya és invàlid.',
    'user' => "No s'ha trobat cap usuari amb aquest e-mail.",

];
