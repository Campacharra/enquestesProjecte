<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
            'name' => 'administrador',
            'email' => 'administrador@gmail.com',
            'password' => bcrypt('123456'),
            'edit' => true,
            'super' => true,
            'admin' => true,
            ]);
        DB::table('users')->insert([
        	'name' => 'publicador',
        	'email' => 'publicador@gmail.com',
        	'password' => bcrypt('123456'),
        	]);
        DB::table('users')->insert([
        	'name' => 'editor',
        	'email' => 'editor@gmail.com',
        	'password' => bcrypt('123456'),
            'edit' => true,
        	]);
        DB::table('users')->insert([
        	'name' => 'supervisor',
        	'email' => 'supervisor@gmail.com',
        	'password' => bcrypt('123456'),
            'super' => true,
        	]);
    }
}
