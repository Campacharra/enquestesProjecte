<?php

use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->insert(['name' => 'A']);
        DB::table('groups')->insert(['name' => 'B']);
        DB::table('groups')->insert(['name' => 'C']);
        DB::table('groups')->insert(['name' => 'D']);
    }
}
