<?php

use Illuminate\Database\Seeder;

class ConceptsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('concepts')->insert(['name' => 'Català']);
        DB::table('concepts')->insert(['name' => 'Castellà']);
        DB::table('concepts')->insert(['name' => 'Anglès']);
        DB::table('concepts')->insert(['name' => 'Matemàtiques']);
        DB::table('concepts')->insert(['name' => 'Ciències socials']);
        DB::table('concepts')->insert(['name' => 'Ciències naturals']);
        DB::table('concepts')->insert(['name' => 'Tecnologia']);
        DB::table('concepts')->insert(['name' => 'Música']);
        DB::table('concepts')->insert(['name' => 'Visual i plàstica']);
        DB::table('concepts')->insert(['name' => 'Informàtica']);
       	DB::table('concepts')->insert(['name' => 'Salut i esport']);
       	DB::table('concepts')->insert(['name' => 'Física i química']);
       	DB::table('concepts')->insert(['name' => 'Biologia i geologia']);
       	DB::table('concepts')->insert(['name' => 'Ciències del món']);
       	DB::table('concepts')->insert(['name' => 'Educació física']);
    }
}
