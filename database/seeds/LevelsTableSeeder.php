<?php

use Illuminate\Database\Seeder;

class LevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('levels')->insert(['name' => '1er']);
        DB::table('levels')->insert(['name' => '2on']);
        DB::table('levels')->insert(['name' => '3er']);
        DB::table('levels')->insert(['name' => '4rt']);
    }
}
