<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(['name' => 'ESO']);
        DB::table('categories')->insert(['name' => 'BATX']);
        DB::table('categories')->insert(['name' => 'CFGM']);
        DB::table('categories')->insert(['name' => 'CFGS']);
    }
}

