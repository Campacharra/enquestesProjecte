<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_id')->unsigned()->nullable();
            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
            $table->integer('annex_question_id')->unsigned()->nullable();
            $table->foreign('annex_question_id')->references('id')->on('annex_questions')->onDelete('cascade');
            $table->integer('order')->unsigned()->nullable();
            $table->text('option_text'); 
            $table->boolean('other')->default(false);
            $table->text('numeric_min_text')->nullable();
            $table->text('numeric_max_text')->nullable(); 
            $table->integer('rating')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('options');
    }
}
