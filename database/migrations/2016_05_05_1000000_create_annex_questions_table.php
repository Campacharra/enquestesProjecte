<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnexQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annex_questions', function (Blueprint $table){
            $table->increments('id');
            $table->integer('published_survey_id')->unsigned();
            $table->foreign('published_survey_id')->references('id')->on('published_surveys')->onDelete('cascade');
            $table->enum('question_type', ['single', 'multiple', 'text', 'numeric']);
            $table->integer('order')->unsigned()->nullable();
            $table->text('question_text');      
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('annex_questions');
    }
}

