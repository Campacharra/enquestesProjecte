$(document).ready(function () {

	$('.btn-delete-user').click(function () {
		var row = $(this).parents('tr');
		var userId = row.data('id');
		$.get('user/delete/' + userId, function (response) {
			if (response.user.id == userId) {
				row.fadeOut();
			}
		});
	});

    $('.btn-active-survey').click(function (){
        var btn = $(this);
        var row = $(this).closest('tr');
        var surveyId = row.data('id');
        $.get('survey/setactive/' + surveyId, function (response) {
            if(response.survey.active){
                btn.text('Si');            
            } else {
                btn.text('No');
            }
        });
    });



     $('[data-toggle="tooltip"]').tooltip();   
     /*
     reloadActiveButton();

     setInterval(function(){
        reloadActiveButton();
     },1000);
    */
    
    
       
    /*Corregir vista del donut con pestanyas*/    
        $( ".tab-2" ).each(function() {
            $id = $(this).attr('href');
            $($id).removeClass('in');
            $($id).removeClass('active');
        });

    
        
    
});

function showAnswerType(sel, action) {
    var type= sel.value;

    $(document.getElementById("options-"+action)).addClass("hidden");
    $(document.getElementById("numeric-"+action)).addClass("hidden");
    $(".option").remove();
    
    
    if (type == "multiple" || type == "single" || type == "single_other"){
        $("#options-"+action).removeClass("hidden");
        addOption(0, "disabled", action);
        if (type == "single_other"){
            addOptionOther(action);
        }
    } else if(type == 'numeric'){
        $(document.getElementById(type+"-"+action)).removeClass("hidden");
    }
	
}

function addOption(num, $disabled, action, text){
    $(".answer-options-"+action).append('<div class="option form-group input-group">'
								    +'<input class="form-control" name="option_text_'+num+'[]" type="text" required>'
                                    +'<span class="input-group-btn">'
								        +'<button type="button" class="btn btn-primary" onclick="removeOption(this)" '+$disabled+'>-</button>'
                                    +'</span>'
								+'</div>');

}

function addOptionOther(action){
    $(".other-option-"+action).append('<div class="option form-group">'
                                    +'<label>opció de resposta de text:'
                                        +'<input class="form-control" name="option_text_other" type="text" value="Altres:" required >'
                                    +'</label>'
                                +'</div>');

}

function removeOption(btn){
  btn.closest('.option').remove();
}





function addAnnexQuestion() {
    
    var annexQuestionNum;

    if( $('.annex-question').size() < 2){
        if (document.getElementById("annex-question-1")) {
            annexQuestionNum = 2;
        } else {
            annexQuestionNum = 1;
        }
        $("#annex-questions").append(
                                    '<div class="form-group annex-question">'
                                        + '<div class="panel panel-default">'
                                            + '<div class="panel-heading clearfix">'
                                                + '<div class="panel-title pull-left" >Pregunta </div>'
                                                + '<div class="pull-right">'
                                                    + '<button type="button" class="btn btn-sm btn-danger" onclick="removeAnnexQuestion(this)">eliminar</button>'
                                                + '</div>'
                                            + '</div>'
                                            + '<div id="annex-question-'+annexQuestionNum+'" class="panel-body">'
                                                + '<input type="hidden" name="annex-question-'+annexQuestionNum+'" value="'+annexQuestionNum+'">'
                                                + '<div class="form-group">'
                                                    + '<label for="question" class="control-label">Pregunta:</label>'
                                                    + '<input type="text" class="form-control" name="question-'+annexQuestionNum+'" required>'
                                                + '</div>'
                                                + '<div class="form-group">'
                                                    + '<label for="answer-type" class="control-label">Tipus de resposta:</label>'
                                                    + '<select name="answer-type-'+annexQuestionNum+'" id="answerTypeSelection" class="form-control" onchange="showAnswerType(this)" required>'
                                                        + '<option value="">Tria un tipus de resposta</option>'
                                                        + '<option value="text">Resposta de text</option>'
                                                        + '<option value="single">Opció de resposta única</option>'
                                                        + '<option value="multiple">Opció de resposta múltiple</option>'
                                                        + '<option value="numeric">Puntuació</option>'
                                                    + '</select>'
                                                + '</div>'
                                                + '<div id="options" class="form-group hidden">'
                                                    + '<div class="panel panel-default">'
                                                        +'<div class="panel-heading clearfix">'
                                                            +'<h3 class="panel-title pull-left" >Opcions</h3>'
                                                            +'<button type="button" class="btn btn-primary btn-sm pull-right" onclick="addOption('+annexQuestionNum+')">afegir</button>'
                                                        +'</div>'
                                                        +'<div class="panel-body">'
                                                            +'<div class="answer-options">'
                                                            +'</div>'
                                                        +'</div>'
                                                    +'</div>'
                                                +'</div>'
                                                +'<div id="numeric" class="form-group hidden">'
                                                    +'<label for="rating-type" class="control-label">Puntuació sobre:</label>'
                                                    +'<select name="rating-type-'+annexQuestionNum+'" id="ratingSelection" class="form-control">'
                                                        +'<option value=1>1</option>'
                                                        +'<option value=2>2</option>'
                                                        +'<option value=3>3</option>'
                                                        +'<option value=4>4</option>'
                                                        +'<option value=5 selected>5</option>'
                                                        +'<option value=6>6</option>'
                                                        +'<option value=7>7</option>'
                                                        +'<option value=8>8</option>'
                                                        +'<option value=9>9</option>'
                                                        +'<option value=10>10</option>'
                                                    +'</select>'
                                                    +'<label for="min-text" class="control-label">Text per a puntuació mínima:</label>'
                                                    +'<input type="text" class="form-control" name="min-text-'+annexQuestionNum+'"/>'
                                                    +'<label for="max-text" class="control-label">Text per a puntuació màxima:</label>'
                                                    +'<input type="text" class="form-control" name="max-text-'+annexQuestionNum+'"/>'
                                                +'</div>'
                                            +'</div>'
                                        +'</div>'

                                    +'</div>'
        );
    }
    disableAnnexQuestionBtn();
};

function removeAnnexQuestion(btn){
    btn.closest('.annex-question').remove();
    disableAnnexQuestionBtn();
}

function disableAnnexQuestionBtn(){
    if( $('.annex-question').size() == 2){
        $('#add-annex-question-btn').addClass("disabled");
    } else {
        $('#add-annex-question-btn').removeClass("disabled");
    }
}
/*
function reloadActiveButton(){
    $('.btn-active-survey').load('/enquestes/public/admin');
}
*/


