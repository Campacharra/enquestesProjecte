<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Función para determinar si un usuario tiene privilegios para publicar encuestas
     */
    public function isPublisher() {
        return $this->publish;
    }

    /**
     * Función para determinar si un usuario tiene privilegios para editar o crear encuestas
     */
    public function isEditor() {
        return $this->edit;
    }

    /**
     * Función para determinar si un usuario tiene privilegios para editar o crear encuestas
     */
    public function isSupervisor() {
        return $this->super;
    }

    /**
     * Función para determinar si un usuario tiene privilegios para editar o crear encuestas
     */
    public function isAdmin() {
        return $this->admin;
    }

    // One To Many con encuestas activas
    public function publishedSurveys() {
        return $this->hasMany('App\PublishedSurvey');
    }
}
