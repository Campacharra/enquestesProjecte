<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
	// una pregunta tiene diferentes opciones como respuesta
    public function options() {
    	return $this->hasMany('App\Option');
    }

    public function answers() {
    	return $this->hasMany('App\Answer');
    }

    public function survey() {
        return $this->belongsTo('App\Survey');
    }

}
