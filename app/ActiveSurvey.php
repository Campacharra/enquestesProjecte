<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActiveSurvey extends Model
{    
    public function survey() {
        return $this->belongsTo('App\ActiveSurvey');
    }
    
    // Many To One con usuarios
    public function user() {
    	return $this->hasOne('App\User');
    }

    // Many To One con conceptos
    public function concept() {
    	return $this->hasOne('App\Concept');
    }

    // Many To One con grupos
    public function group() {
    	return $this->hasOne('App\Group');
    }

    // Many To One con niveles
    public function level() {
    	return $this->hasOne('App\Level');
    }

    // One To Many con intentos
    public function intents() {
        return $this->hasMany('App\Intent');
    }

    // One To Many con respuestas
    public function answers() {
        return $this->hasMany('App\Answer');
    }
}
