<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnnexQuestion extends Model
{
    // una pregunta tiene diferentes opciones como respuesta
    public function options() {
    	return $this->hasMany('App\Option');
    }

    public function answers() {
    	return $this->hasMany('App\Answer');
    }

    public function publishedSurvey() {
        return $this->belongsTo('App\PublishedSurvey');
    }
}
