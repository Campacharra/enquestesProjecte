<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    public function questions() {
    	return $this->hasMany('App\Question');
    }

    public function category() {
    	return $this->belongsTo('App\Category');
    }

    public function publishedSurveys() {
    	return $this->hasMany('App\PublishedSurvey');
    }
}
