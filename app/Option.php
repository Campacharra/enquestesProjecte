<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    public function question() 
    {
        return $this->belongsTo('App\Question');
    }

    public function countAnswers(PublishedSurvey $pubSurvey) 
    {
    	$answers = Answer::where(['published_survey_id'=>$pubSurvey->id, 'question_id'=>$this->question->id, 'answer'=>$this->id])->get();
		return $answers->count(); 
	}

	public function annexQuestion() {
    	return $this->belongsTo('App\AnnexQuestion');
    }
}
