<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function surveys() {
    	return $this->hasMany('App\Survey');
    }

    public function isUsed(){
    	$surveys = $this->surveys;
    	if (count($surveys)){
    		return true;
    	}
	    return false;
    }
}
