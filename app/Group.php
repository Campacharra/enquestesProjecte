<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
 	// One To Many con encuestas activas
    public function publishedSurveys() {
    	return $this->hasMany('App\PublishedSurvey');
    }

    public function isUsed(){
    	$pSurveys = $this->publishedSurveys;
    	if (count($pSurveys)){
    		return true;
    	}
	    return false;
    }
}
