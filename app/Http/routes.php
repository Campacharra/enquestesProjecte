<?php

use App\User;
use App\Http\Middleware\RoleMiddleware;
use App\Http\Middleware\AdminMiddleware;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::post('/publishsurvey/{survey}', 'PublishedSurveyController@publish');
Route::post('/resendsurvey/{pubSurvey}', 'PublishedSurveyController@resend');
Route::post('/accesssurvey', 'PublishedSurveyController@access');
Route::get('/viewsurvey/{pubSurvey}', 'PublishedSurveyController@result');
Route::resource('/filtersurvey', 'PublishedSurveyController@filter');
Route::get('editsurvey/{survey}', 'CreateSurveyController@viewSurvey');

Route::get('/admin', 'UserController@administrate');

Route::group(['middleware' => AdminMiddleware::class], function() {

	Route::post('/updatekey', 'SettingsController@key');

	Route::get('/user/delete/{id}', 'UserController@destroy');

	Route::post('/user/update/{user}', 'UserController@update');
});

Route::group(['middleware' => RoleMiddleware::class], function() {

	Route::get('/create', 'CreateSurveyController@getCategories');

	Route::get('/survey/{survey}', 'CreateSurveyController@viewSurvey');

	Route::post('/createnew', 'CreateSurveyController@saveSurvey');
	Route::post('savequestion/{survey}', 'CreateSurveyController@saveQuestion');
	Route::get('deletequestion/{question}', 'CreateSurveyController@deleteQuestion');

	Route::get('deletesurvey/{survey}', 'CreateSurveyController@deleteSurvey');
	Route::get('obsoletesurvey/{survey}', 'CreateSurveyController@setObsolete');
	

	Route::get('/survey/setactive/{survey}', 'CreateSurveyController@setActive');

	Route::post('saveanswer/{pubSurvey}/{intent}', 'AnswerController@save');

	Route::post('/createcategory', 'CategoryController@save');
	Route::get('deletecategory/{category}', 'CategoryController@delete');
	Route::post('editcategory/{category}', 'CategoryController@edit');

	Route::post('/createconcept', 'ConceptController@save');
	Route::get('deleteconcept/{concept}', 'ConceptController@delete');
	Route::post('editconcept/{concept}', 'ConceptController@edit');

	Route::post('/createlevel', 'LevelController@save');
	Route::get('deletelevel/{level}', 'LevelController@delete');
	Route::post('editlevel/{level}', 'LevelController@edit');

	Route::post('/creategroup', 'GroupController@save');
	Route::get('deletegroup/{group}', 'GroupController@delete');
	Route::post('editgroup/{group}', 'GroupController@edit');

	Route::post('editquestion/{question}', 'CreateSurveyController@editQuestion');
	Route::post('/updatesurvey/{oldSurvey}', 'CreateSurveyController@updateSurvey');

});

Route::get('edituser', array('middleware' => 'auth', function(){
    return view('edituser');
}));

Route::post('editusername', 'UserController@editUserName');

Route::post('edituseremail', 'UserController@editEmail');

Route::post('edituserpassword', 'UserController@editPassword');
