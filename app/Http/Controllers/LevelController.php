<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Level;

class LevelController extends Controller
{

	/**
     * Create a new controller instance.
     *
     * @return void
     */

	public function __construct(){
        $this->middleware('auth');
    }
    
    public function save(Request $request){
        $level; 
        
        $level = Level::where('name', $request->input('name'))->first();
        
        if($level !== null){
            return redirect()->back()->withErrors(['unique'=>'Aquest nivell ja existeix']);
        } else {
            $level = new Level();
            $level->name = $request->input('name');
            $level->save();
            return redirect()->action('UserController@administrate');
        }
        
    }

    public function delete(Level $level){
        $level->delete();
         return redirect()->action('UserController@administrate');
    }
    
    public function edit(Request $request, Level $level){
        $level = Level::where('name', $request->input('name'))->first();
        
        if($level !== null){
            return redirect()->back()->withErrors(['unique'=>'Aquest nivell ja existeix.']);
        } else {
            $level-> name = $request->input('name');
            $level->save();
            return redirect()->action('UserController@administrate');
        }
    }
}
