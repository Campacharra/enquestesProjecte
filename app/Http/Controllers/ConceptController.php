<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Concept;

class ConceptController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    public function save(Request $request){
        $concept; 
        
        $concept = Concept::where('name', $request->input('name'))->first();
        
        if($concept !== null){
            return redirect()->back()->withErrors(['unique'=>'Aquesta assignatura ja existeix']);
        } else {
            $concept = new Concept();
            $concept->name = $request->input('name');
            $concept->save();
            return redirect()->action('UserController@administrate');
        }        
    }

    public function delete(Concept $concept){
    	$concept->delete();
    	return redirect()->action('UserController@administrate');    
    }
    
    public function edit(Request $request, Concept $concept){
         $concept = Concept::where('name', $request->input('name'))->first();
        
        if($concept !== null){
            return redirect()->back()->withErrors(['unique'=>'Aquesta assignatura ja existeix.']);
        } else {
            $concept-> name = $request->input('name');
            $concept->save();
            return redirect()->action('UserController@administrate');
        }
    }
}
