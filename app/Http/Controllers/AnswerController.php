<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\PublishedSurvey;
use App\Intent;
use App\Question;
use App\Answer;
use App\Option;

class AnswerController extends Controller
{
    
    public function save(Request $request, PublishedSurvey $pubSurvey, Intent $intent){
        
        $questions = $request->except('_token', 'other');
        
        foreach ($questions as $input => $value) { 
                
            if (is_array($value)){
                foreach($value as $val){               
                    $answer = new Answer();
                    $answer->answer = $val;
                    $answer->publishedSurvey()->associate($pubSurvey);
                    $question = Question::find($input);
                    $answer->question()->associate($question);
                    $answer->save();        
                }
            } else{
                $answer = new Answer();
                $answer->answer = $value;
                $question = Question::find($input);
                $answer->publishedSurvey()->associate($pubSurvey);
                $answer->question()->associate($question);  
                if ($question->question_type == 'single_other'){
                        $option = Option::find($value);
                        if ($option->other){
                            $answer->answer_other = $request->input('other');
                        }
                    }         
                $answer->save();
            }
        }

        $pubSurvey->count += 1;
        $pubSurvey->save();
        
        $intent->delete();
        
        return redirect('/')->with('success', 'Enquesta contestada correctament, gràcies per participar.');;
    }
    
}
