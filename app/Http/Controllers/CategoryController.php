<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Category;

class CategoryController extends Controller
{
    
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function save(Request $request){ 
        $category = Category::where('name', $request->input('name'))->first();
        
        if($category !== null){
            return redirect()->back()->withErrors(['unique'=>'Aquesta categoria ja existeix']);
        } else {
            $category = new Category();
            $category->name = $request->input('name');
            $category->save();
            return redirect()->action('UserController@administrate');
        }
        
    }

    public function delete(Category $category){
        $category->delete();
         return redirect()->action('UserController@administrate');
    }
    
    public function edit(Request $request, Category $category){
        
        $category = Category::where('name', $request->input('name'))->first();
        
        if($category !== null){
            return redirect()->back()->withErrors(['unique'=>'Aquesta categoria ja existeix.']);
        } else {
            $category-> name = $request->input('name');
            $category->save();
            return redirect()->action('UserController@administrate');
        }
    }
}
