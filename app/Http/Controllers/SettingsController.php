<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Validator;

use App\Setting;

class SettingsController extends Controller
{
    public function key(Request $request)
    {
    	/*
	 	$this->validate($request, [
            'privateKey' => 'required|min:8|max:16',
        ]);
*/
        $validator = Validator::make($request->all(), [
            'privateKey' => 'required|min:8|max:16',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $privateKey = Setting::where(['key' => 'global'])->first();
        $privateKey->value = $request->input('privateKey');
        $privateKey->save();

        return redirect()->back()->with(['editkeysuccess'=>"Clau d'accés editada correctament"]);
    }
}
