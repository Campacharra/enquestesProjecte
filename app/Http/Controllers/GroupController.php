<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Group;

class GroupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

	public function __construct(){
        $this->middleware('auth');
    }
    
    public function save(Request $request){
        $group; 
        
        $group = Group::where('name', $request->input('name'))->first();
        
        if($group !== null){
            return redirect()->back()->withErrors(['unique'=>'Aquest grup ja existeix']);
        } else {
            $group = new Group();
            $group->name = $request->input('name');
            $group->save();
            return redirect()->action('UserController@administrate');
        }
        
    }

    public function delete(Group $group){
        $group->delete();
         return redirect()->action('UserController@administrate');
    }
    
    public function edit(Request $request, Group $group){
        $group = Group::where('name', $request->input('name'))->first();
        
        if($group !== null){
            return redirect()->back()->withErrors(['unique'=>'Aquest grup ja existeix.']);
        } else {
            $group-> name = $request->input('name');
            $group->save();
            return redirect()->action('UserController@administrate');
        }
    }

}
