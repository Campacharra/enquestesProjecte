<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;


use Illuminate\Support\Facades\Auth;
use App\PublishedSurvey;
use App\User;
use App\Survey;
use App\Concept;
use App\Level;
use App\Group;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $auth = Auth::user();
        if ($auth->super == false) {
            $pubSurveys = PublishedSurvey::where('user_id', $auth->id)->get();
        } else {
            $pubSurveys = PublishedSurvey::all();
        }
        if ($auth->super == false) {
            $users = array(User::find($auth->id));
        } else {
            $users = User::all();
        }
        $surveys = Survey::where(['active' => true, 'outdated' => false])->get();
        $concepts = Concept::all();
        $levels = Level::all();
        $groups = Group::all();
        
        return view('/home', (['pubSurveys'=>$pubSurveys, 'users'=>$users, 'surveys'=>$surveys, 'concepts'=>$concepts, 'levels'=>$levels, 'groups'=>$groups]));
    }
}
