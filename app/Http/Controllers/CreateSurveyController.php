<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Survey;
use App\Question;
use App\Option;
use App\Category;

class CreateSurveyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }
    
    public function viewSurvey(Survey $survey) {

        if(Auth::user()->edit || Auth::user()->publish) {

            if($survey->publishedSurveys->count() == 0){
                $survey->active = false;
                $survey->save();
            }
            return view('/createquestions')->with('survey', $survey);
        } else {
            return redirect()->back();
        }
    }

    public function saveSurvey(Request $request) {

            $survey = new Survey();
            $survey->name = $request->input('name');
            $category = Category::find($request->input('survey-type'));
            $category->surveys()->save($survey);
            //$survey->category()->associate($category);
            //$survey->save();

        return redirect()->action('CreateSurveyController@viewSurvey', [$survey->id]);
        //return view('/createquestions')->with('survey', $survey);
    }

    public function saveQuestion (Request $request, Survey $survey) {

        if ($survey->publishedSurveys->count()) {
            return redirect()->action('UserController@administrate')->withErrors(['msg' => 'Aquesta enquesta ha estat publicada. No es por editar.']);
        }

        $question = new Question();

        $question->survey()->associate($survey);
        $question->question_type = $request->input('answer-type');
        $question->question_text = $request->input('question');
        $question->save();

        if($request->input('answer-type') == 'single' || $request->input('answer-type') == 'multiple' || $request->input('answer-type') == 'single_other'){

            $options = null;
            

            if($request->input('answer-type') == 'single_other'){
                $options = $request->option_text_other;
                $option = new Option();
                $option->question()->associate($question);
                $option->option_text = $options;
                $option->other = true;
                $option->save();
            }

             $options = $request->option_text_0;

            if ( $options != null){
                foreach ($options as $optionText) {
                    $option = new Option();
                    $option->question()->associate($question);
                    $option->option_text = $optionText;
                    $option->save();
                }
            }



        } else if ($request->input('answer-type') == 'numeric') {

            $max = $request->input('rating-type');

            for ($i = 1; $i <= $max; $i++) {

                $option = new Option();

                $option->question()->associate($question);
                $option->option_text = $i;
                $option->rating = $max;
                $option->numeric_min_text = $request->input('min-text');
                $option->numeric_max_text = $request->input('max-text');
                $option->save();    
            }

            /*
            $option = new Option();

            $option->question()->associate($question);
            $option->option_text = $request->input('rating-type');
            $option->numeric_min_text = $request->input('min-text');
            $option->numeric_max_text = $request->input('max-text');
            $option->save();
            */

        }
        return redirect()->action('CreateSurveyController@viewSurvey', [$survey->id]);
        //return view('/createquestions')->with('survey', $survey);
    }

    public function editQuestion (Request $request, Question $question) {

        if ($question->survey->publishedSurveys->count()) {
            return redirect()->action('UserController@administrate')->withErrors(['msg' => 'Aquesta enquesta ha estat publicada. No es por editar.']);
        } else {
            $survey = $question->survey;
            $survey->active = false;
            $survey->save();
        }


        $question->question_type = $request->input('answer-type');
        $question->question_text = $request->input('question');
        $question->options()->delete();
        $question->save();

        if($request->input('answer-type') == 'single' || $request->input('answer-type') == 'multiple' || $request->input('answer-type') == 'single_other'){

            $options = null;
            

            if($request->input('answer-type') == 'single_other'){
                $options = $request->option_text_other;
                $option = new Option();
                $option->question()->associate($question);
                $option->option_text = $options;
                $option->other = true;
                $option->save();
            }

             $options = $request->option_text_0;

            if ( $options != null){
                foreach ($options as $optionText) {
                    $option = new Option();
                    $option->question()->associate($question);
                    $option->option_text = $optionText;
                    $option->save();
                }
            }



        } else if ($request->input('answer-type') == 'numeric') {

            $max = $request->input('rating-type');

            for ($i = 1; $i <= $max; $i++) {

                $option = new Option();

                $option->question()->associate($question);
                $option->option_text = $i;
                $option->rating = $max;
                $option->numeric_min_text = $request->input('min-text');
                $option->numeric_max_text = $request->input('max-text');
                $option->save();    
            }
        }
        return redirect()->action('CreateSurveyController@viewSurvey', [$survey->id]);
        //return view('/createquestions')->with('survey', $survey);
    }

    function deleteQuestion(Question $question) {
      $survey = $question->survey;
      $question->delete();
      return redirect()->action('CreateSurveyController@viewSurvey', [$survey->id]);
    }

    function getCategories() {
      $categories = Category::all();

      return view('/createsurvey')->with('categories', $categories);
    }
    
    function deleteSurvey(Survey $survey) {
        $survey->delete();
        return redirect()->action('UserController@administrate');
    }

    function setObsolete(Survey $survey) {
        $survey = Survey::find($survey->id);
        $survey->outdated = true;
        $survey->save();
        return redirect()->action('UserController@administrate');
    }

    public function setActive(Survey $survey) {

        if($survey->active){
            $survey->active = false;
        } else {
            $survey->active = true;
        }
        $survey->save();
        return response()->json(['survey' => $survey]);
    }

    public function updateSurvey(Request $request, Survey $oldSurvey){
        $oldSurvey->active = false;
        $oldSurvey->outdated = true;
        $oldSurvey->save();

        $newSurvey = new Survey();
        $newSurvey->name = $request->input('name');
        $category = Category::find($request->input('survey-type'));
        $category->surveys()->save($newSurvey);


        foreach($oldSurvey->questions as $oldquestion){
            $newQuestion = new Question();
            $newQuestion->survey()->associate($newSurvey);
            $newQuestion->question_type = $oldquestion->question_type;
            $newQuestion->question_text = $oldquestion->question_text;
            $newQuestion->save();
            
            foreach($oldquestion->options as $oldOption){
                $newOption = new Option();
                $newOption->question()->associate($newQuestion);
                $newOption->option_text = $oldOption->option_text;
                $newOption->other = $oldOption->other;
                $newOption->numeric_min_text = $oldOption->numeric_min_text;
                $newOption->numeric_max_text = $oldOption->numeric_max_text;
                $newOption->rating = $oldOption->rating;
                $newOption->save();

            }
        }

        $oldSurvey->active = false;
        $oldSurvey->outdated = true;
        $oldSurvey->save();

         return redirect()->action('CreateSurveyController@viewSurvey', [$newSurvey->id]);
    }
}