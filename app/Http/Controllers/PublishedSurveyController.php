<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Survey;
use App\User;
use App\Concept;
use App\Category;
use App\Group;
use App\Level;
use App\PublishedSurvey;
use App\Intent;
use App\AnnexQuestion;

use Mail;

class PublishedSurveyController extends Controller
{
	/*
	 * Recibe los datos del formulario mediante $request y un objeto $survey
	 */
    public function publish(Request $request, Survey $survey) 
    {		
        // controlar que la encuesta no est� desactivada
        if (!$survey->active) {
            return redirect()->back()->withErrors(['msg' => 'Aquesta enquesta ha estat desactivada!']);
        }

        // insertar registro en encuestas publicadas
        $user = User::find($request->input('user'));
        $concept = Concept::find($request->input('concept'));
        $group = Group::find($request->input('group'));
        $level = Level::find($request->input('level'));
        $pubSurvey = new PublishedSurvey();
        $pubSurvey->survey()->associate($survey);
        $pubSurvey->user()->associate($user);
        $pubSurvey->concept()->associate($concept);
        $pubSurvey->group()->associate($group);
        $pubSurvey->level()->associate($level);
        $pubSurvey->save();

    	// crear array de emails
    	$emails = explode(";", $request->input('emails'));
    	// validar emails
    	foreach ($emails as $email => $value) {

    		if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
                // crear clave
                $key = bin2hex(openssl_random_pseudo_bytes(4));
                // vista, datos, closure
                Mail::send('emails.access', ['key' => $key], function ($m) use ($value) {
                    $m->from('enquestes@app.com', 'Enquestes');
                    $m->to($value)->subject('Enquesta disponible');
                });
                $intent = new Intent();
                $intent->email = $value;
                $intent->key = $key;
                $intent->publishedSurvey()->associate($pubSurvey);
                $intent->save();
			}
    	}
        return back();
    }

    public function resend(Request $request, PublishedSurvey $pubSurvey) 
    {
        // crear array de emails
        $emails = explode(";", $request->input('emails'));
        // validar emails
        foreach ($emails as $email => $value) {

            if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
                // crear clave
                $key = bin2hex(openssl_random_pseudo_bytes(4));
                // vista, datos, closure
                Mail::send('emails.access', ['key' => $key], function ($m) use ($value) {
                    $m->from('enquestes@app.com', 'Enquestes');
                    $m->to($value)->subject('Enquesta disponible');
                });
                $intent = new Intent();
                $intent->email = $value;
                $intent->key = $key;
                $intent->publishedSurvey()->associate($pubSurvey);
                $intent->save();
            }
        }
        return back();
    }

    public function access(Request $request) 
    {
        $key = $request->input('key');
        $key = str_replace(' ', '', $key);
        $intent = Intent::where('key', '=', $key)->first();

        if ($intent === null) {
            return back()->withErrors(['msg', 'error']);
        } else {
            $pubSurvey = PublishedSurvey::find($intent->publishedSurvey->id);
            return view('survey', (['pubSurvey'=>$pubSurvey, 'intent'=>$intent]));
        }
    }

    public function result(PublishedSurvey $pubSurvey)
    {
        $auth = Auth::user();

        $filters = array();

        if ($auth->super == false) {
            $filters['user_id'] = $auth->id;
        }
      
        // parte importante: aqu� se cargan todas las encuestas publicadas aplicando los filtros
        $pubSurveys = PublishedSurvey::where($filters)->get();
    
        if ($auth->super == false) {
            $users = array(User::find($auth->id));
        } else {
            $users = User::all();
        }
        $concepts = Concept::all();
        $categories = Category::all();
        $levels = Level::all();
        $groups = Group::all();

        if ($pubSurvey->control == false && $pubSurvey->user != Auth::user()) {

            $pubSurvey->control = true;
            $pubSurvey->save();

            $address = $pubSurvey->user->email;
            Mail::send('emails.visit', ['pubSurvey' => $pubSurvey], function ($m) use ($address) {
                $m->from('enquestes@app.com', 'Enquestes');
                $m->to($address)->subject('Enquesta visitada');
            });
        }

        return view('result', (['pubSurveys'=>$pubSurveys, 'selectedSurvey'=>$pubSurvey, 'users'=>$users, 'concepts'=>$concepts, 'categories'=>$categories, 'levels'=>$levels, 'groups'=>$groups]));
    }

    public function filter(Request $request) 
    {
        $auth = Auth::user();

        $filters = array();

        // $request solo cuando se usa el formulario para filtrar
        // utilizo user pero podr�a usar cualquier otro campo del formulario
        if ($request->has('user')) {
            if ($request->input('user') > 0) {
                $filters['user_id'] = $request->input('user');
            }  

            if ($request->input('concept') > 0) {
                $filters['concept_id'] = $request->input('concept');
            }

            if ($request->input('level') > 0) {
                $filters['level_id'] = $request->input('level');
            }

            if ($request->input('group') > 0) {
                $filters['group_id'] = $request->input('group');
            }

            if ($request->input('category') > 0) {
                $filters['category_id'] = $request->input('category');
            }
        } else {
            if ($auth->super == false) {
                $filters['user_id'] = $auth->id;
            }
        }

        // parte importante: aqu� se cargan todas las encuestas publicadas aplicando los filtros
        $pubSurveys = PublishedSurvey::join('surveys', 'published_surveys.survey_id', '=', 'surveys.id')->where($filters)->get();

        if ($pubSurveys->isEmpty()) {
            return back()->withErrors(['msg' => "No s'han trobat enquestes segons els criteris establerts. Es retorna la llista completa."]);
        }
    
        if ($auth->super == false) {
            $users = array(User::find($auth->id));
        } else {
            $users = User::all();
        }
        $concepts = Concept::all();
        $categories = Category::all();
        $levels = Level::all();
        $groups = Group::all();

        $selectedSurvey = $pubSurveys->first();

        if ($selectedSurvey->control == false && $selectedSurvey->user != Auth::user()) {

            $selectedSurvey->control = true;
            $selectedSurvey->save();

            $address = $selectedSurvey->user->email;
            Mail::send('emails.visit', ['pubSurvey' => $selectedSurvey], function ($m) use ($address) {
                $m->from('enquestes@app.com', 'Enquestes');
                $m->to($address)->subject('Enquesta visitada');
            });
        }

        return view('result', (['pubSurveys'=>$pubSurveys, 'selectedSurvey'=>$selectedSurvey, 'users'=>$users, 'concepts'=>$concepts, 'categories'=>$categories, 'levels'=>$levels, 'groups'=>$groups]));
    }

    public function saveAnnexQuestion(Request $request, PublishedSurvey $pubSurvey, $num){
        $annexQuestion = new AnnexQuestion();

        $annexQuestion->publishedSurvey()->associate($pubSurvey);
        $annexQuestion->question_type = $request->input('answer-type-'.$num);
        $annexQuestion->question_text = $request->input('question-'.$num);
        $annexQuestion->save();

        if($request->input('answer-type-'.$num) == 'single' || $request->input('answer-type-'.$num) == 'multiple'){

            $options = null;
            $options = $request->input('option_text_'.$num);

            if ( $options != null){
                foreach ($options as $optionText) {
                    $option = new Option();
                    $option->annexQuestion()->associate($annexQuestion);
                    $option->option_text = $optionText;
                    $option->save();
                }
            }

        } else if ($request->input('answer-type') == 'numeric'){

            $option = new Option();

            $option->annexquestion()->associate($annexQuestion);
            $option->option_text = $request->input('rating-type-'.$num);
            $option->numeric_min_text = $request->input('min-text'.$num);
            $option->numeric_max_text = $request->input('max-text'.$num);
            $option->save();
        }
    }
}
