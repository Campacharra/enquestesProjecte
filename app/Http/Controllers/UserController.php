<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Response;

use App\Setting;
use App\User;
use App\Category;
use App\Concept;
use App\Group;
use App\Level;
use App\Survey;

class UserController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

	public function destroy($id) {
		$user = User::find($id);
		$user->delete();
		return response()->json(['user' => $user]);
	}

	public function update(Request $request, User $user) {
		
		$user->publish = ($request->input('publish'))? true : false;
		$user->edit = ($request->input('edit'))? true : false;
		$user->super = ($request->input('super'))? true : false;
		$user->admin = ($request->input('admin'))? true : false;
		$user->save();
		return redirect()->action('UserController@administrate');
	}

	// TODO: mover a otro controlador
	public function administrate() {

		if (!Auth::user()->edit && !Auth::user()->admin) {
			return redirect()->back();
		}

		$privateKey = Setting::where(['key' => 'global'])->get();
		$users = User::all();
		$categories = Category::all();
		$concepts = Concept::all();
		$groups = Group::all();
		$levels = Level::all();
		$surveys = Survey::all();
		return view('/admin', ['privateKey'=>$privateKey, 'users'=>$users, 'categories'=>$categories, 'concepts'=>$concepts, 'groups'=>$groups, 'levels'=>$levels, 'surveys'=>$surveys]);
	}
    
    public function editUserName(Request $request) {

    	
    	
        $this->validate($request, [
            'name' => 'required',
        ]);
        
        Auth::user()->name = $request->input('name');
        Auth::user()->save();

        return redirect()->back()->with(['editnamesuccess'=>'Nom editat correctament']);    
            
    }
    
    public function editEmail(Request $request) {
        
        if (Auth::user()->email != $request->input('email')){
            $this->validate($request, [
                'email' => 'required|unique:users',

            ]);
        }
        
        Auth::user()->email = $request->input('email');
        Auth::user()->save();

        return redirect()->back()->with(['editnaemailsuccess'=>'email editat correctament']);    
            
    }
    
    public function editPassword(Request $request) {


 
        $this->validate($request, [
            'password' => 'required|min:6|confirmed',


        ]);
/*
        if($request->input('old-password')!= Auth::user()->password){
            return redirect()->back()->withErrors(['badpwd'=>'la contrasenya es incorrecta']);
        }
  */      
        
        Auth::user()->password = bcrypt($request->input('password'));
        Auth::user()->save();

        return redirect()->back()->with(['editpwdsuccess'=>'Contrasenya editada correctament']);     
            
    }

}
