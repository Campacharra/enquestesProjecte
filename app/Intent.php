<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Intent extends Model
{
    public function publishedSurvey() {
    	return $this->belongsTo('App\PublishedSurvey');
    }
}
