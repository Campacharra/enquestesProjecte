<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DateTime;

class PublishedSurvey extends Model
{
    public function survey() {
        return $this->belongsTo('App\Survey');
    }
    // Many To One con usuarios
    public function user() {
    	return $this->belongsTo('App\User');
    }

    // Many To One con conceptos
    public function concept() {
    	return $this->belongsTo('App\Concept');
    }

    // Many To One con grupos
    public function group() {
    	return $this->belongsTo('App\Group');
    }

    // Many To One con niveles
    public function level() {
    	return $this->belongsTo('App\Level');
    }

    // One To Many con intentos
    public function intents() {
        return $this->hasMany('App\Intent');
    }

    // One To Many con respuestas
    public function answers() {
        return $this->hasMany('App\Answer');
    }
    public function annexQuestions() {
        return $this->hasMany('App\AnnexQuestion');
    }
}
